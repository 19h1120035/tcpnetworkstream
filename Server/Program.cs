﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;


namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Tcp NetworkStream Server";
            Socket listener = new Socket(SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(new IPEndPoint(IPAddress.Any, 1308));
            listener.Listen(10);
            Console.WriteLine($"Server started at {listener.LocalEndPoint}");
            while (true)
            {
                Socket worker = listener.Accept();
                // khởi tạo object của NetworkStream từ tcp socket
                NetworkStream stream = new NetworkStream(worker);
                // sử dụng bộ tích hợp streamreader để đọc chuỗi truy vấn
                // vì truy vấn ỏ dạng chuỗi (theo quy ước giữa client và server), StreamReader sẽ giúp đọc
                // chuỗi byte từ luồng NetworkStream và từ động chuyển đổi ký tự utf-8
                StreamReader sr = new StreamReader(stream);

                // Sử dụng bộ tiếp học StreamWriter để ghi chuỗi phản hồi vào luồng NetworkStream
                // vì phản hồi ở dạng chuỗi theo quy ước giữa client và server, StreamWriter sẽ giúp biến đổi chuỗi ký tự 
                // utf-8 thành mảng byte và đưa vào luồng NetworkStream
                // Chú ý sử dựng AutoFlush = true để tự động flush luồng NetworkStream
                StreamWriter sw = new StreamWriter(stream) { AutoFlush = true };

                // Đọc thông qua bộ tiếp học thay vì đọc trực tiếp chuỗi byte từ luồng NetworkStream
                // phương thức ReadLine của StreamReader sẽ tự làm việc với NetworkStream bên trong để đọc ra chuỗi byte,
                // sau đó tự động biến đổi chuỗi utf-8 .
                // lưu ý: lệnh ReadLine sẽ đọc đến khi nào nhìn thấy cặp ký tự \r\n thì sẽ dừng lại.
                // Trong kết quả trả về, ReadLine sẽ xóa bỏ hay ký tự thừa này.
                // Vì vậy, ở client chúng ta phải bổ sung cặp \r\n vào cuối chuỗi truy vấn.
                // Nếu không làm như vậy, ReadLine sẽ không dừng việc đọc,.
                string request = sr.ReadLine();
                string response = string.Empty;
                switch (request.ToLower())
                {
                    case "date":
                        response = DateTime.Now.ToString("dd/MM/yyyy");
                        break;
                    case "time":
                        response = DateTime.Now.ToLongTimeString();
                        break;
                    case "year":
                        response = DateTime.Now.Year.ToString();
                        break;
                    case "month":
                        response = DateTime.Now.Month.ToString();
                        break;
                    case "day":
                        response = DateTime.Now.Day.ToString();
                        break;
                    case "dow":
                        response = DateTime.Now.DayOfWeek.ToString();
                        break;
                    case "doy":
                        response = DateTime.Now.DayOfYear.ToString();
                        break;
                    default:
                        response = "UNKNOW COMMAND";
                        break;
                }
                // ghi thẳng chuối utf-8 vào luồng bằng phương thức WriteLine của StreamReader thay vì tự biến đổi chuỗi sang mảng byte
                // phương thức WriteLien tự thêm cặp \r\n vào cuỗi chuỗi, tự đông biến đổi chuỗi này thành mảng byte và ghi vào stream
                // vì lí do này, bên client sẽ phải tự mình cắt bỏ chuỗi \r\n vì ReadLine sẽ tự động xóa bỏ cặp kí tự này giúp
                sw.WriteLine(response);

                // nếu không sử dụng AutoFlush thì phải tự mình gọi lện Flush
                // writer.Flush();
                // đóng socket sẽ dóng toàn bộ các luồng liên quan
                worker.Close();
            }
        }
    }
}
