﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Tcp NetworkStream Client";
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Server IP: ");
            IPAddress address = IPAddress.Parse(Console.ReadLine());
            IPEndPoint serverEndPoint = new IPEndPoint(address, 1308);
            while (true)
            {
                Console.Write("# Command >> ");
                string request = Console.ReadLine();
                Socket client = new Socket(SocketType.Stream, ProtocolType.Tcp);
                client.Connect(serverEndPoint);

                // Khởi tạo biến của NetworkStream từ tcp socket
                NetworkStream stream = new NetworkStream(client);
                // lưu ý bổ sung \r\n vào cuối chuỗi truy vấn
                // do bên server sẽ sử dụng phương thức ReadLine của bộ tiếp hợp StreamReader
                // nếu không bổ sung \r\n thì ReadLine sẽ không thể dừng việc đọc
                byte[] sendBuffer = Encoding.UTF8.GetBytes(request + "\r\n");
                // ghi mảng byte vào stream thay vì dùng phương thức send của socket
                stream.Write(sendBuffer, 0, sendBuffer.Length);
                // yều cầu stream đẩy dữ liệu đi ngay
                // nếu không có lệnh này, dữ liệu sẽ nằm tại bộ nhớ tạm và tcp socket sẽ không nhận được 
                stream.Flush();

                byte[] receiveBuffer = new byte[1024];
                // đọc mảng byte từ stream thay vì dùng phương thức receive của socket
                var count = stream.Read(receiveBuffer, 0, 1024);
                string response = Encoding.UTF8.GetString(receiveBuffer, 0, count);
                // lưu ý cắt bỏ các kí tự thừa \r\n ở cuối chuỗi phản hồi trước khi in ra console
                // do bên server sẽ sử dụng phương thức WriteLine của bộ tiếp hợp StreamWriter
                // tất cả chuỗi gửi lên đường truyền đều tự động bổ sung cặp \r\n vào cuối, do đó phải cắt bỏ đi trước khi in ra
                Console.WriteLine(response.Trim());

                // đóng socket cũng đồng nghĩa với đóng và xóa bỏ các object của networkStream tạo ra từ object này
                // chúng ta cũng có thể gọi stream.Close() thay vì đóng socket. Khi đóng luồng mạng sẽ tự động đóng socket bên dưới
                client.Close();

            }
        }
    }
}
